import QtQuick 2.0
import QtQuick.Particles 2.0


Rectangle {
    color: "#000040"
    width: 1000; height: 800
    
    Tree {
        width: 500
        height: 500
        anchors.right: parent.right
        anchors.bottom: parent.bottom
    }
  
    ParticleSystem {
        id: sys
        running: true
        anchors.fill: parent
        
        Emitter {
            anchors.left: parent.left
            anchors.bottom: parent.top
            anchors.right: parent.right
            height: 1
            id: emitter
            emitRate:  50
            size: 20
            lifeSpan: 1000*120 //one minute

            velocity: CumulativeDirection {
                AngleDirection {angle: 90; magnitude: 15; magnitudeVariation: 10}
            }
        }
        
        Gravity {
            anchors.fill: parent
            magnitude: 10
        }        
                
        Friction {
//             strength: 4000000;
            factor: 100
            threshold: 8
            anchors.centerIn: parent
            width: 405
            height:277
            enabled: true
            shape: MaskShape {
                source: "31_no_bold.png"
            }
        }

        MouseArea {
            anchors.fill: parent
            Attractor {
                anchors.centerIn: parent
                strength: 200
                enabled: parent.pressed
            }
        }
        
//         Attractor {
//             strength: 4000
//             anchors.centerIn: parent
//             width: 405
//             height:277
//             shape: MaskShape {
//                 source: "/home/david/temp.png"
//             }
//             
//         }

        
        Wander {
            id: wanderer
            system: sys
            anchors.fill: parent
            xVariance: 5
            pace: 100*(wanderer.affectedParameter+1);
        }
        
        ImageParticle {
            source: "qrc:///particleresources/star.png"
        }
    }
    

}