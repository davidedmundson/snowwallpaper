import QtQuick 2.0
import QtQuick.Particles 2.0


MouseArea {   
    onClicked: mainAnim.start()

    Path {
        id: myPath
        startX: 270
        startY: 500
        PathCurve {x:270; y: 470}
        PathCurve {x:100; y: 450}
        PathCurve {x:400; y: 432}
        PathCurve {x:115; y: 414}
        PathCurve {x:385; y: 396}
        PathCurve {x:130; y: 378}
        PathCurve {x:370; y: 360}
        PathCurve {x:145; y: 342}
        PathCurve {x:355; y: 324}
        PathCurve {x:160; y: 306}
        PathCurve {x:340; y: 288}
        PathCurve {x:175; y: 270}
        PathCurve {x:325; y: 252}
        PathCurve {x:190; y: 234}
        PathCurve {x:310; y: 216}
        PathCurve {x:205; y: 198}
        PathCurve {x:295; y: 180}
        PathCurve {x:220; y: 162}
        PathCurve {x:280; y: 144}
        PathCurve {x:235; y: 126}
        PathCurve {x:265; y: 108}
    }
    
    SequentialAnimation 
    {
        id: mainAnim
        loops: 1
        running: false
       
        ScriptAction {
            script: emitter.enabled = true;
        }
        PathAnimation {
            id: foo
            path: myPath
            target: emitter
            duration: 2000
        }
        PauseAnimation {
            duration: 40 //let emitter emit some at the top
        }
        ScriptAction {
            script: emitter.enabled = false;
        }

        PauseAnimation {
            duration: 4000 //let all particles die
        }
    }
    
    ParticleSystem {
        id: sys
        running: mainAnim.running
        anchors.fill: parent
        
        Emitter {
            id: emitter
            emitRate:  2000
            size: 4
            endSize: 0
            lifeSpan: 1000
            lifeSpanVariation: 4000
            enabled: false

            velocity: CumulativeDirection {
                AngleDirection {angle: 0; angleVariation: 360; magnitude: 0; magnitudeVariation: 6;}
            }
        }
        ImageParticle {
            source: "qrc:///particleresources/glowdot.png"
            entryEffect: ImageParticle.Fade
            alpha: 0.0
        }
    }
}